from django.db import models

# Create your models here.
class Author(models.Model):
    author = models.CharField(max_length = 200)
    def __str__(self):
        return self.author

class Category(models.Model):
    name = models.CharField(max_length = 200)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='parent_category')
    def __str__(self):
        return self.name

class Tag(models.Model):
    tag = models.CharField(max_length = 200)
    def __str__(self):
        return self.tag

class News(models.Model):
    header = models.CharField(max_length=200, null=True)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)
    date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    def __str__(self):
        return self.header

    class Meta:
        ordering = ('date',)

