from django.shortcuts import render
from .serializers import NewsSerializer
from .models import News
from rest_framework import generics
from rest_framework import viewsets

class NewsViewSet(viewsets.ModelViewSet):
    serializer_class = NewsSerializer
    queryset = News.objects.all()