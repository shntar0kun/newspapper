from django.contrib import admin
from .models import News, Author, Tag, Category
# Register your models here.

@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    fields = (
        'header',
        'author', 
        'category', 
        'tags',
        'text'
    )

admin.site.register(Category)
admin.site.register(Author)
admin.site.register(Tag)
