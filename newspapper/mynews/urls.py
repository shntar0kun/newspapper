from django.conf.urls import url
from .views import NewsViewSet
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'news', NewsViewSet)
urlpatterns = router.urls