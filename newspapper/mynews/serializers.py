from rest_framework import serializers
from .models import News, Category, Author, Tag
from rest_framework_recursive.fields import RecursiveField

class CategorySerializer(serializers.ModelSerializer):
    parent = RecursiveField(read_only=True)
    class Meta:
        model = Category
        fields = ('name', 'parent')

class NewsSerializer(serializers.ModelSerializer):
    recursive_category = CategorySerializer(source='category')

    author = serializers.SlugRelatedField(
        queryset=Author.objects.all(),
        slug_field='author'
     )
    tags = serializers.SlugRelatedField(
        many=True,
        queryset=Tag.objects.all(),
        slug_field='tag'
     )
    class Meta:
        model = News
        fields = ('id', 'author', 'category', 'recursive_category', 'tags', 'header', 'text')

