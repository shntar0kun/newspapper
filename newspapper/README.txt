To install PostgreSQL:
sudo apt install postgresql-9.5 postgresql-server-dev-9.5

To install 'logging' module run: easy_install logging.

To make this project work install all requirements using command 'pip install -r requirements.txt' in your virtual environment and set database settings in newspapper/local_settings.py
Then run 'python manage.py migrate' to create database tables.
After that you should create a superuser using command 'python manage.py createsuperuser'.